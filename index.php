<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Bitly</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
    <div class="header">
        <nav class="navbar navbar-light">
            <div class="grid">
                <a href="" class="navbar__logo">
                    <img src="images/logo1.png" alt="logo">
                    <h2>Bitly</h2>
                </a>
                <div class="navbar__list">
                    <a href="#home" id="home"> HOME</a>
                    <a href="#services" id="services"> SERVICES</a>
                    <a href="#about" id="about"> ABOUT</a>
                    <a href="contact.php" id="contact"> CONTACT US</a>
                    <a href="#" id="search"><i class="fa fa-search"></i></a>
                    <input type="search" placeholder="Search your favoirite here..." class="search-bar" id="search-bar">
                    <a id="close-icon" href="#" style="display:none;">X</a>
                </div>
                <div class="u-pull-right">
                    <a href="login.php" class="login">LOGIN</a>
                    <a href="signup.php" class="signup">SIGN UP</a>
                </div>
            </div>

            <a href="#" class="burger" id="burger">
                <div class="lin"></div>
                <div class="lin"></div>
                <div class="lin"></div>
            </a>

            <div class="burger-nav" id="burger-nav">
                <a href="#home"  class="burger-nav__list" id="home"> HOME</a>
                <a href="#services"  class="burger-nav__list" id="services"> SERVICES</a>
                <a href="#about" class="burger-nav__list"  id="about"> ABOUT</a>
                <a href="contact.php"  class="burger-nav__list" id="contact"> CONTACT US</a>
                <div class="login__signup">
                    <a href="login.php" class="burger-nav__list burger-nav__login">LOGIN</a><br>
                    <a href="signup.php" class="burger-nav__list burger-nav__signup">SIGN UP</a>
                </div>
            </div>
        </nav>
        <div class="firstSection">
            <div class="box-main">
                <div class="firstHalf">
                    <p class="text-big">
                        Simpler <b>Social Media</b> tool for authentic engagement
                    </p>
                    <br>
                    <p class="text-small">
                        Bitly is a cloud CRM platform designed to help you build and strengthen relationships through every step of the customer journey. Let’s grow together.
                    </p>
                    <br><br>
                    <div class="boxmain-signup">
                        <a href="signup.php">START NOW</a>
                    </div>
                </div>
                <div class="secondHalf">
                    <img src="images/social3.png" alt="">
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    $(document).ready(function() {
        $("#burger-nav").hide()
        $("#search").click(function() {
            $("#home, #services, #about, #contact, #search").hide(100);
            $("#search-bar, #close-icon").show(100);
        });
        $("#close-icon").click(function() {
            $("#search-bar, #close-icon").hide();
            $("#home, #services, #about, #contact, #search").show(200);
        });
        $("#burger").click(function() {
            $("#burger-nav").toggle(200);
        });
    });
</script>

</html>




//    Author: Rohit Tiwari
//    Created: 14 july 2020 09:13 AM