<?php
session_start();
if ($_SESSION["loggedin"] != true) {
    header("location: login.php");
}
if (isset($_GET['logout'])) {
    session_reset();
    session_destroy();
    header("location: login.php");
}

if (isset($_SESSION['loggedin'])) {
    $email = $_SESSION['email'];
    $username = $_SESSION['username'];
    require '_dbconnect.php';
    $sql = "call sel_tblUserPosts('" . $email . "')";
    if (isset($conn)) {
        $result = mysqli_query($conn, $sql);
        $PostCounts = mysqli_num_rows($result);
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="css/profile.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title><?php echo $_SESSION['username']; ?> | Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

    <nav class="navbar">
        <div class="navbar-logo">
            <a href="" class="navbar__logo">Bitly</a>
        </div>
        <div class="u-pull-right">
            <div class="username"><i class="fa fa-user-o" aria-hidden="true"></i> <?php echo $username; ?></div>
            <div class="logout"><a href="profile.php?logout=true">LOGOUT</a></div>
        </div>
    </nav>

    <div class="sidenav">
        <ul class="sidenav__menu">
            <li class=" sidenav_newPost">
                <a href="#newPost" class="newPost">New Post</a>
            </li>
            <li class="sidenav__menuitem">
                <div class="menuColor">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                    Posts
                </div>
                <ul class="sidenav_submenu">
                    <li class="menuColor">All (<?php echo $PostCounts; ?>)</li>
                    <li class="menuColor">Published (<?php echo $PostCounts; ?>)</li>
                </ul>
            </li>
            <li class="sidenav__menuitem menuColor">
                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                Stats
            </li>
            <li class="sidenav__menuitem menuColor">
                <i class="fa fa-comments-o" aria-hidden="true"></i>
                Comments
            </li>
            <li class="sidenav__menuitem menuColor">
                <i class="fa fa-cogs" aria-hidden="true"></i>
                Settings
            </li>
        </ul>
    </div>

    <div class="main-body">

        <div class="posts_control-menu">
            <div class="posts_control-list">
                <input class="posts_control" type="checkbox" name="" id="" onClick="toggle(this)">
                <button class="publish posts_control">Publish</button>
                <button class="draft posts_control">Revert to Draft</button>
                <button class="delete posts_control">Delete</button>
            </div>
            <hr>
        </div>

        <div class="posts">
            <?php
            while ($row = $result->fetch_array()) {
                echo '<div class="user_posts">';
                echo '<input class="posts_control" type="checkbox" name="foo" value="#" class="" id="">';
                echo '<li ><a target="_blank" href="' . $row['link'] . '">' . $row['title'] . '</a></li>';
                // echo '<img style="" src="data:image/jpeg;base64,' . base64_encode($row['image']) . '"/>';
                echo '<div class="posts_control_right">';
                echo '<h1>' . $username . '</h1>';
                echo '<div class="right_column"><br>' . $row['views'] . ' <i class="fa fa-eye" aria-hidden="true"></i></div>';
                echo '</div>';
                echo '</div>';
                echo '<hr>';
            }
            ?>
        </div>
    </div>

    <div class="newpost_form">
        <input type="file" class="frm-input">
        <input type="text" class="frm-input">
        <input type="text" class="frm-input">
        <input type="text" class="frm-input">
    </div>

</body>

<script language="JavaScript">
    function toggle(source) {
        checkboxes = document.getElementsByName('foo');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = source.checked;
        }
    }
</script>

</html>


//    Author: Rohit Tiwari
//    Created: 14 July 9:06AM
